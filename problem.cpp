#include <iostream>
#include <string>
#include <stdexcept>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

int main(int argc, char **argv){
    try{
	if (argc != 2){
	    throw std::runtime_error("usage: ./EXECUTABLE INPUTFILENAME");
	}
	std::ifstream infile(argv[1],std::ifstream::in);
	std::string sentence = result = "";
	int found; 

	while (!infile.eof()){
	    getline(infile,sentence);
	    std::transform(sentence.begin(),sentence.end(),sentence.begin(),::tolower);
	    found = sentence.find("problem");
	    result = (found != std::string::npos) ? "yes":"no";
	    std::cout << result << std::endl;
	}
    }
    catch(std::exception &e){
	std::cerr << "Caught exception: " << e.what() << std::endl;
	exit(1);
    }
}
